<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpress' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'gEmjrVwGG[ $+rZ]{1n=?{30ub9q)eGVA=vWb[p@E8;{#RPf<Q1Ujt7-^5U.0AW^' );
define( 'SECURE_AUTH_KEY',  ']z+%uf8L277A^uGRb0=jjRe*p4L8oI25&4g9,FHt,X}qgR3j(#qF<l`o/,I{A(aM' );
define( 'LOGGED_IN_KEY',    'XM~Z9Zvqn.g~2|zKSW9fnG)j]!%rl)%_T3Z| -~wSj)H=|<KFurya*&mso:RH5x_' );
define( 'NONCE_KEY',        '-au>HLJ,&8W`a.jAhsJ``=R8]4@{$N^q;!t<#NE:y2Q_vsU!#bn#Y0rBv>;h@Y$O' );
define( 'AUTH_SALT',        '9K!CtYQiqXJ:i?UlQk~5u._)BAK</I& >71F<=Ov~>O:Er)zfzx>~2z8C?qv01gq' );
define( 'SECURE_AUTH_SALT', '6HPEe$,:.Q qVd_.U&~tGq]I=Q1ej^s*Y+PLsnly qrjUjL$~g.;JR=KXZwlQ+1%' );
define( 'LOGGED_IN_SALT',   'RddTA#E!`)c6w v*5%(9SI|WkM#W:%+u~dA#vJv/2P6&Rz7fpK>p.f=>kjAQ.~!;' );
define( 'NONCE_SALT',       '&dfo_RD!vv@c}Qf[]Dyrjw8QOEWoau`LE+X^Wjyv?[x?X=$EJ~,?72e0AXUL0pc~' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
